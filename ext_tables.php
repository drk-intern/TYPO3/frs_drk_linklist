<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

// do not use "use" for \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider due to TYPO3 caching bug!

use \TYPO3\CMS\Core\Utility\GeneralUtility;

// does not work if copying to TCA/Overrides/tt_content
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
    'tx_frsdrklinklist_domain_model_main'
);

$iconRegistry = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon('frs_linklist-ce-drilldown', \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class, array(
    'source' => 'EXT:frs_drk_linklist/Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_Drilldown.svg'
));
