<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "frs_drk_linklist"
 *
 * Auto generated by Extension Builder 2015-07-06
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Linklist',
	'description' => 'Implements the linklist on the DRK homepage.',
	'category' => 'plugin',
	'author' => 'Kjeld Schumacher',
	'author_email' => 'systeme@familie-redlich.de',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '1.0.0',
	'constraints' => array(
		'depends' => array(
			'typo3' => '7.6.0-7.6.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);
