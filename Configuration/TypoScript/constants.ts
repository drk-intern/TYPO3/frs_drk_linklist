plugin.tx_frsdrklinklist {
    view {
        # cat=plugin.tx_drkjobboard/file; type=string; label=Path to template root (FE)
        templateRootPaths = EXT:frs_drk_linklist/Resources/Private/Templates/
        # cat=plugin.tx_drkjobboard/file; type=string; label=Path to template partials (FE)
        partialRootPaths = EXT:frs_drk_linklist/Resources/Private/Partials/
        # cat=plugin.tx_drkjobboard/file; type=string; label=Path to template layouts (FE)
        layoutRootPaths = EXT:frs_drk_linklist/Resources/Private/Layouts/
    }
}
