<?php
namespace Frs\FrsDrkLinklist\Controller;

    /*
     *    _______________
     *    |       .-.   |
     *    |      // ``  |
     *    |     //      |
     *    |  == ===-_.-'|
     *    |   //  //    |
     *    |__//_________|
     *
     * Copyright (c) 2016 familie-redlich :systeme <systeme@familie-redlich.de>
     *
     * @link     http://www.familie-redlich.de
     * @package  DRK
     *
     */

/**
 * Main
 */
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;

class MainController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * @var \Frs\FrsDrkLinklist\Domain\Repository\MainRepository
     * @inject
     */
    protected $mainRepository;

    /**
     * @var \Frs\FrsDrkLinklist\Domain\Repository\PagesRepository
     * @inject
     */
    protected $pagesRepository;

    /**
     * show action
     *
     * @return void
     */
    public function showAction()
    {
        // get the content objects
        $data = $this->configurationManager->getContentObject()->data;
        $pages = $this->mainRepository->findByContentElementUid($data['uid']);
        $this->view->assign('pages', $pages);
        $this->view->assign('icon', $pages['icon']);
        $this->view->assign('header', $data['header']);
        $this->view->assign('header_layout', $data['header_layout']);
        $this->view->assign('content', $data['bodytext']);
    }

    /**
     * Injects the view
     *
     * Note: This function is intended for unit-testing purposes only
     *
     * @param \TYPO3\CMS\Extbase\Mvc\View\ViewInterface $view
     *
     * @return $view
     */
    public function setView(ViewInterface $view)
    {
        $this->view = $view;
    }
}
