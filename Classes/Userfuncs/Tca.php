<?php
namespace Frs\FrsDrkLinklist\Userfuncs;

/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2016 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */

use \TYPO3\CMS\Backend\Utility\BackendUtility;

/**
 * Class Tca
 *
 * @package FrsDrkLinklist\Userfuncs
 */
class Tca
{

    /**
     * Gets the title from the referenced page
     *
     * @param array $parameters Parameters used to identify the current record
     * @param object $parentObject Calling object
     *
     * @return void
     */
    public function getReferencedPageTitle(&$parameters, $parentObject)
    {
        $pageId = $parameters['row']['links'];
        // fix for multiple calls of this function. Why does TYPO3 call this function 9 times for only 3 elements.
        if (strpos($pageId, '|')) {
            list($pages, $name) = explode('|', $pageId);
            $parameters['title'] = urldecode($name);
        }
    }
}
